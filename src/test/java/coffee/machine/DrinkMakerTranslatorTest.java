package coffee.machine;

import static org.assertj.core.api.Assertions.assertThat;
import coffee.machine.order.Order;
import coffee.machine.order.Type;
import org.junit.jupiter.api.Test;

class DrinkMakerTranslatorTest {
    @Test
    void should_translate_tea_with_one_sugar() {
        //given
        DrinkMakerTranslator drinkMakerTranslator = new DrinkMakerTranslator();

        final var result = drinkMakerTranslator.translate(new Order(Type.TEA));

        assertThat(result)
            .isEqualTo("T::");
    }
}
